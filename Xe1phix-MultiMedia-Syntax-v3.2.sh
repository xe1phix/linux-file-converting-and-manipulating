



## Convert a Nero .nrg file to .iso
## Nero simply adds a 300Kb header to a normal iso image. This can be trimmed with dd.
dd bs=1k if=imagefile.nrg of=imagefile.iso skip=300



# Burn existing iso image
growisofs ‐dvd‐compat ‐Z /dev/dvd=imagefile.iso


## Burn directly
growisofs ‐dvd‐compat ‐Z /dev/dvd ‐J ‐R /p/to/data




cdrecord dev=ATAPI ‐scanbus

cdrecord ‐scanbus
# To find the burner device (like 1,0,0)



Create and burn an ISO image
mkisofs ‐J ‐L ‐r ‐V TITLE ‐o imagefile.iso /path/to/dir





Convert video
Compress the Canon digicam video with an mpeg4 codec and repair the crappy sound.
# mencoder ‐o videoout.avi ‐oac mp3lame ‐ovc lavc ‐srate 11025 \
‐channels 1 ‐af‐adv force=1 ‐lameopts preset=medium ‐lavcopts \
vcodec=msmpeg4v2:vbitrate=600 ‐mc 0 vidoein.AVI





Copy an audio cd
The program cdparanoia http://xiph.org/paranoia/ can save the audio tracks (FreeBSD port in audio/cdparanoia/), oggenc can
encode in Ogg Vorbis format, lame converts to mp3.
#
#
#
#
cdparanoia ‐B
# Copy the tracks to wav files in current dir
lame ‐b 256 in.wav out.mp3
# Encode in mp3 256 kb/s
for i in *.wav; do lame ‐b 256 $i `basename $i .wav`.mp3; done
oggenc in.wav ‐b 256 out.ogg
# Encode in Ogg Vorbis 256 kb/s













