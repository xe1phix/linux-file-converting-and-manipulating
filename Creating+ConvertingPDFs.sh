#!/bin/sh
###########
## Creating+ConvertingPDFs.sh



echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
echo "    [+] Convert Text File --> PDF File:      "
echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
pandoc test.txt -o test.pdf

pandoc -s -t -o socat-cli.pdf socat-cli.txt

echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
echo "    [+] Convert HTML --> TEXT:         "
echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
pandoc -s -o $Output.txt $Input.html


echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
echo "    [+] EPUB to plain text:"
echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
pandoc $Input.epub -t plain -o $Output.txt


echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
echo "    [+] Convert Text File --> PDF File:      "
echo "##-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-##"
pandoc test.txt -o test.pdf


pdf2txt Unix\ Toolbox.pdf > Unix-Toolbox.txt


Create a pdf version of a manpage
man -t manpage | ps2pdf - filename.pdf

man 7 -t capabilities | ps2pdf - > capabilities.pdf



convert unixtoolbox.pdf unixtoolbox‐%03d.png 
convert *.jpeg images.pdf          		# Create a simple PDF with all pictures 
convert image000* ‐resample 120x120 ‐compress JPEG ‐quality 80 images.pdf 


PDF Creation and Manipulation:

## ============================================================================================================================ ##
pdf2txt -o output.html samples/naacl06-shinyama.pdf					## Extract text as an HTML file whose filename is output.html:
## ============================================================================================================================ ##
pdf2txt -c euc-jp -D tb-rl -o output.html samples/jo.pdf			## Extract a Japanese HTML file in vertical writing:
## ============================================================================================================================ ##
pdf2txt -P mypassword -o output.txt secret.pdf						## Extract text from an encrypted PDF file:
## ============================================================================================================================ ##
dumppdf -a test.pdf													## Dump all the headers and contents, except stream objects:
## ============================================================================================================================ ##
dumppdf -T test.pdf													## Dump the table of contents:
## ============================================================================================================================ ##

dumppdf -r -i6 test.pdf > image.jpeg								## Extract a JPEG image:


## ============================================================================================================================ ##

man -t ascii | ps2pdf - > ascii.pdf								## make a pdf of a manual page ##

man 7 -t capabilities | ps2pdf - > capabilities.pdf

pdftotext -layout 


cat 

apropos 

/usr/share/man/index.(bt|db|dir|pag)
A traditional global index database cache.

/var/cache/man/index.(bt|db|dir|pag)
An FHS compliant global index database cache.

/usr/share/man/.../whatis
A traditional whatis text database.

unzipped by gunzip, piped to nroff then finally piped to less: 
gunzip < /usr/share/man/man1/grep.1.gz | nroff -c -man | less


zmore document.gz

ps2pdf14

Converting a figure.ps to figure.pdf:

ps2pdf figure.ps

A conversion with more specifics:

ps2pdf -dPDFSETTINGS=/prepress figure.ps proof.pdf

Converting as part of a pipe:

make_report.pl -t ps | ps2pdf -dCompatibility=1.3 - - | lpr

/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/linux-lpic-guide-v4.pdf
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/Linux-Filesystem-Hierarchy.pdf
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/unixtoolbox.pdf
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/comptia-linux-LX0-104(lx0-104)-aug-39-14-version.pdf
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/abs-guide.pdf
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/"Linux Bible 2010 Edition.pdf"
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/"McGraw.Hill.HackNotes.Linux.and.Unix.Security.Portable.Reference.eBook-DDU.pdf"
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/"The LPIC-2 Exam Prep.pdf"
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/"RHCSA RHCE Red Hat Linux Certification Study Guide Seventh Edition Exams EX 200  EX 300.pdf"
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/Pocket-Linux-Guide.pdf
/usr/bin/atril /home/faggot/Browntown/LPI-10[3-4]-LPIC/""
/usr/bin/atril /home/faggot/Browntown/textbooks/debian-handbook.pdf
/usr/bin/atril /home/faggot/Browntown/textbooks/"Hardening Linux.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/"Hacking Exposed Linux, 3rd Edition.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/"Chrome keyboard shortcuts - Chrome Help.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/Debian Developers Documentation/"
/usr/bin/atril /home/faggot/Browntown/textbooks/Linux-Dictionary.pdf
/usr/bin/atril /home/faggot/Browntown/textbooks/"List of Chromium Command Line Switches « Peter Beverloo.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/paranoid-security-guide.pdf
/usr/bin/atril /home/faggot/Browntown/textbooks/Securing-Optimizing-Linux-The-Ultimate-Solution-v2.0.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/"Iptables Tutorial 1.2.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV2/"LPI Linux Certification In A Nutshell 3rd Edition V413HAV.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV2/8a5j2.Rtfm.Red.Team.Field.Manual.pdf
/home/faggot/Browntown/textbooks/Cheatsheets/linux/LINUX Admin Quick Reference.pdf
/home/faggot/Browntown/textbooks/Cheatsheets/linux/regular_expressions_cheat_sheet.pdf
/home/faggot/Browntown/textbooks/Cheatsheets/linux/

/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV2/

/home/faggot/Browntown/textbooks/Cheatsheets/linux/bash_ref.pdf
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV3/
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV3/"Linux Firewalls V413HAV.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV3/"Linux Timesaving Techniques for Dummies (ISBN - 0764571737).pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV3/"SELinux Policy Administration - Vermeulen, Sven-signed.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV3/"The Art of Assembly Language, Second Edition.pdf"
/usr/bin/atril /home/faggot/Browntown/textbooks/TextbooksV3/"The Art of Memory Forensics.pdf"

/home/faggot/Browntown/textbooks/Linux/"Syngress - Hack Proofing Linux (2001).pdf"
pluma "/home/faggot/Browntown/textbooks/jon schipp/pcapworksheet.txt"
PostScript  (PS), Encapsulated PostScript (EPS)

convert unixtoolbox.pdf unixtoolbox‐%03d.png 
convert *.jpeg images.pdf          		# Create a simple PDF with all pictures 
convert image000* ‐resample 120x120 ‐compress JPEG ‐quality 80 images.pdf 
mogrify ‐format png *.ppm          # convert all ppm images to png format

convert 20140416‐DSCF1915.jpg 20140416‐DSCF1920.jpg all.pdf 
convert 20140416‐DSCF1915.jpg 20140416‐DSCF1920.jpg ‐resize 1240x1753 ‐units PixelsPerInch ‐density 150x150 all.pdf

echo "Ghostscript can also concatenate multiple pdf files into a single one."
gs ‐q ‐sPAPERSIZE=a4 ‐dNOPAUSE ‐dBATCH ‐sDEVICE=pdfwrite ‐sOutputFile=all.pdf file1.pdf file2.pdf ...


gs ‐q ‐sPAPERSIZE=a4 ‐dNOPAUSE ‐dBATCH ‐sDEVICE=pdfwrite ‐sOutputFile=/path/file.pdf





Converting a figure.ps to figure.pdf:

ps2pdf figure.ps

A conversion with more specifics:

ps2pdf -dPDFSETTINGS=/prepress figure.ps proof.pdf

Converting as part of a pipe:

make_report.pl -t ps | ps2pdf -dCompatibility=1.3 - - | lpr







parse-bkdg.pl < bkdg-raw.data > bkdg.data

info --location

info --subnodes -o out.txt

info info-stnd

--subnodes
--index-search=
--apropos=

unzipped by gunzip, piped to nroff then finally piped to less: 
gunzip < /usr/share/man/man1/grep.1.gz | nroff -c -man | less


make_report.pl -t ps | ps2pdf -dCompatibility=1.3 - - | lpr



info --subnodes -o out.txt




