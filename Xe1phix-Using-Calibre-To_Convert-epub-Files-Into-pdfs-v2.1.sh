#!/bin/sh
## clear && ./Calibre.sh | boxes -d twisted | less
echo
echo " ##-=====================================================-##"
echo "     [+] Converting .epub files into pdfs using Calibre"
echo " ##-=====================================================-##"
echo
echo
echo "   +----------------------------------------------+"
echo "      [1] Open the Calibre GUI"
echo "   +----------------------------------------------+"
echo "      [2] Add the .epub file - select add books"
echo "   +----------------------------------------------+"
echo "      [3] click the convert books menu button"
echo "   +----------------------------------------------+"
echo "      [4] Set the output format to pdf"
echo "   +----------------------------------------------+"
echo "      [5] Start the conversion process"
echo "   +----------------------------------------------+"
echo "      [6] Success!"
echo "   +----------------------------------------------+"
echo
echo
echo "   ##-===============================================-##"
echo "   +------------------------------------------------+"
echo "      [?] The .pdf file will be placed in 			"
echo "          the calibre folder specified at setup		"
echo "   +------------------------------------------------+"
echo "          (I selected the ~/documents folder)		"
echo "   +------------------------------------------------+"
echo "   ##-===============================================-##"
echo
echo
echo
echo
echo "##-=========================================================================-##"
echo "    [?] You Can Find The Step-By-Step Video I Created For This Topic Here:"
echo "##-=========================================================================-##"
echo
echo "## ------------------------------------------------------------------------- ##"
echo "   [?]  Xe1phix's YouTube Source:                                             "
echo "   [?]  https://www.youtube.com/watch?v=5K0H8jiLEIA                           "
echo "## ------------------------------------------------------------------------- ##"
echo "   [?]  Xe1phix's YouTube Source:                                             "
echo "   [?]  https://archive.org/details/ConvertingEpubFilesIntoPdfsUsingCalibre   "
echo "## ------------------------------------------------------------------------- ##"
cowsay -W 28 -f sodomized-sheep "From Nothing To Being, There is No Logical Bridge."


